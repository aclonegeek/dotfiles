# dotfiles
My personal dotfiles that are managed using [GNU stow](https://www.gnu.org/software/stow/).

# Usage
``` shell
stow alacritty bash fish gammastep git gtk gnupg mako mpv paru sway zathura
```

# Misc
`sudo systemctl enable fstrim.timer` for SSDs.

# [Packages](packages)
